#!/usr/bin/env python
import sys, os, socket, logging
from subprocess import Popen, PIPE
import inotify.adapters as adapters
import inotify.constants as flags
try:
    import configparser as cp
except:
    import ConfigParser as cp

def mail(msg,subject,addrs,sentfrom):
    '''sendmail handler.'''
    m = 'To: '+','.join(addrs)+'\n'
    m = m + 'From: '+sentfrom+'\n'
    m = m + 'Content-Type: text/html; charset=UTF-8\n'
    m = m + 'Subject: [Noticer] '+subject+'\n\n'
    if msg.lower().strip().startswith('<html>'):
        m = m + msg + '\n'
    else:
        m = m + '<html>'+msg+'</html>\n'
    s = Popen(['/usr/sbin/sendmail','-t'],stdin=PIPE)
    s.communicate(m)

def configure():
    script_path =os.path.dirname(os.path.realpath(__file__))
    conf = cp.ConfigParser()
    try:
        conf.read(script_path+'/config.cfg')
    except:
        conf.read(script_path+'/config.cfg.example')
    origin = conf.get("config", "origin")
    dest = conf.get("config", "destination")
    dirs = conf.get("config", "dirs")
    dirs = list(set([x.strip() for x in dirs.split('\n')]))
    return dirs,dest,origin

def main():
    dirs,dest,origin = configure()
    logging.warn('Watching dirs: {}'.format(dirs))

    watch_flags = flags.IN_CREATE | flags.IN_MODIFY | flags.IN_MOVED_FROM | flags.IN_MOVED_TO | flags.IN_DELETE
    i = adapters.InotifyTrees(dirs,mask=watch_flags)

    for event in i.event_gen(yield_nones=False):
       eflags = event[1]
       dir = event[2]
       file = event[3]
       hostname = socket.gethostname()
       logging.debug('Event: {}'.format(event))
       logging.warn('{}/{} changed with flags: {}'.format(dir,file,eflags))

       msg=['Hi,']
       msg+=['']
       msg+=['Looks like {}/{} has been changed.'.format(dir,file)]
       msg+=['Flags are: {}'.format(eflags)]
       msg+=['']
       msg+=['You might want to check that.']
       mail('<br>'.join(msg),'Changed file seen on {}'.format(hostname),[dest],origin)

if __name__ =='__main__':
    main()
